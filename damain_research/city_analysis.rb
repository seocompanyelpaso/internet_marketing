require '../common/serp.rb'
require '../common/csv.rb'
require '../common/utils.rb'
require '../common/http_addr.rb'
require '../common/random.rb'
require 'watir-classic'
require 'fileutils'
require 'nokogiri'



# SEARCH_TXT = "solar water heater"
# SEARCH_TXT = "stump tree removal"
# SEARCH_TXT = "carpet cleaning"
# SEARCH_TXT = "reverse osmosis purifiers"
SEARCH_TXT = "plumber"
CITIES_FN = "./data/top1000uscities_ext.txt"
# CITIES_FN = "./data/top1000uscities_test.txt"
DST = "C:/projects/domain_research/city_analysis"

start_id = nil

if (idx = ARGV.index("-id")) && ((idx + 1) < ARGV.size)
	start_id = ARGV[idx+1].to_i
end

if ARGV.include? "-h"
	puts "ruby city_analysis.rb <options>"
	puts "<options>:"
	puts "-h, help"
	puts "-id N, start processing with N-th city"
	exit
end

class CityAnalysis

	def initialize(args)
		@cities = args[:cities]
		@base = args[:destination]
	
		@serp_browser = Watir::Browser.attach(:title,  /Google/) 
		raise "Failed to attach to google browser" if (@serp_browser == nil)
		

	end
	
	def process(args)
		initialize_analysis(args)

		gaussian = RandomGaussian.new(50, 15)
		header = nil
		@csv.each do |line|
			if line[0] == "id"
				header = line
				next 
			end

			if (args[:start_id] != nil) && (line[0].to_i < args[:start_id])
				next
			end

			city = line[1].gsub(/([a-z])([A-Z])/, '\1 \2').gsub(/\//, ' ')
			
			duration = gaussian.rand.round(1)
			duration = 15 if duration < 15
			break_time = Random.rand(1..10)
			duration += 30 if break_time == 1

			curr_search_text = "#{@search_text} #{city}"
			curr_search_text << " #{line[5]}" if line[6] == "duplicate"

			puts "[#{line[0]}] #{curr_search_text}, wait #{duration} s #{(break_time == 1) ? "  *" : ""}"

			serp_res = @serp.get_results text: curr_search_text

			
			File.open("#{@dst}/#{line[0]} #{curr_search_text}.txt", "w") do |file|
				file.write "[#{curr_search_text}]\n"
				file.write "\n"

				file.write "[CITY]\n"				
				if header != nil
					header.each {|h| file.write "\t#{h};"}
					file.write "\n"
				end
				if line != nil
					line.each {|e| file.write "\t#{e};"}
					file.write "\n"
				end
				file.write "\n"

				file.write "[ENTRIES]\n"
				serp_res[:entries].each {|e| file.puts e}
			end

			sleep(duration)
		end
		
		print "\a"*2
	end
	
	private 
	
		def initialize_analysis(args)
			@search_text = args[:search_text]
			@search_text_fn = @search_text.gsub(/ /, '_')
			
			time = DateTime.parse(Time.now.to_s).strftime("%Y-%m-%d_%H-%M-%S")
			@dst = "#{@base}/#{@search_text_fn}_#{time}"
			if File.directory? @dst
				@dst << "_#{Random.rand(1..10**10)}" 
			end
			FileUtils.mkdir_p @dst
			
			@error_log = Logger.new({fn: "" << @dst << "/" << "__error.log"})
			@serp = Serp.new ({browser: @serp_browser, error_log: @error_log})
			@csv = CSVFile.new({	separator: ";", file: @cities})
		end
end

ca = CityAnalysis.new({	cities: CITIES_FN, destination: DST})
ca.process search_text: SEARCH_TXT, start_id: start_id