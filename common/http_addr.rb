# http:/www.biznes.org.pl/blog
# pre: www
# root: biznes.org.pl
# tld: pl
class HttpAddress
	attr_reader	:domain, :root, :pre, :tld
	def initialize(str)
		@polish_double_domains = ["com.pl", "org.pl", "net.pl", "biz.pl"]
		@str = str
		if @str =~ /^(https?:\/\/)?([a-zA-Z0-9\-]+\.)*([a-zA-Z0-9\-]+\.[a-zA-Z]{2,})(\/.*)?/
			http, pre, root, suffix = $1, $2, $3, $4
			raise "Unexpected http address: #{@str}" if root == nil
			@polish_double_domains.each do |d|
				if root =~ Regexp.new("^#{d}$")
					raise "Unexpected http address: #{@str}" if pre == nil
					if pre =~ /([a-zA-Z0-9\-]+\.)*([a-zA-Z0-9\-]+)/
						pre = $1
						root = "" << $2 << "." << root
					else
						raise "Unexpected http address: #{@str}"
					end
				end
			end
			@domain = "" 
			@domain << pre if pre != nil
			@domain << root
			@pre = pre
			@root = root
			@tld = root.gsub(/^.+\.([a-zA-Z]{2,})$/, '\1')
		else
			raise "Unexpected http address: #{@str}"
		end
	end
	
	def matches?(domain)
		ret = false
		ret = true if @root =~ Regexp.new("^#{domain}$")
		ret
	end
end