
class Whois

	def initialize(args)
		@whois_check_count = 0
		@b = args[:browser]
		@error_log = args[:error_log]
	end

	def check_whois(args)
		
		@whois_data_present = false
		@available_present = false
		@domain = args[:domain]
		
		
		form = @b.form(:id, "whois_lookup_form")
		
		input = form.text_field(:class, "whois-input")
		button = @b.button(:class, "ui-button")
		raise "Can't find search input field" if input == nil
		raise "Can't find search button" if button == nil
		
		input.set @domain
		button.click
		sleep(2)

		
		begin
			@whois_data = @b.div(:id, "registryData").inner_html
			@whois_data_present = true

		rescue Watir::Exception::UnknownObjectException
			begin
				@whois_data = @b.span(:id, "availableText").inner_html
				@available_present = true
			rescue Watir::Exception::UnknownObjectException
				@error_log.log "Unrecognized format of whois page for domain: #{@domain}"
			end
		end

		analyze_whois_data
		
		{whois_data: @whois_data, days_left: @days_left}
	end
				
	private
	
		def analyze_whois_data	
			tld = HttpAddress.new(@domain).tld
			if @whois_data_present == true
				if tld == "pl"
					re = Regexp.new("^.+?(\\d+\\.\\d+\\.\\d+\\s+\\d+:\\d+:\\d+).*") 
					strtime = "%Y.%m.%d %H:%M:%S"
					expiry_field = "expiration date"
				elsif tld == "com"
					re = Regexp.new("^.+?(\\d+-\\w+-\\d+).*") 
					strtime = "%d-%b-%Y"
					expiry_field = "expiration date"					
				elsif tld == "org"
					re = Regexp.new("^.+?(\\d+-\\d+-\\d+T\\d+:\\d+:\\d+Z)") 
					strtime = "%Y-%m-%dT%H:%M:%SZ"
					expiry_field = "registry expiry date"
				else
					re = Regexp.new("^.+?(\\d+-\\w+-\\d+).*") 
					strtime = "%d-%b-%Y"
					expiry_field = "expiration date"
				end
					
				if tld == "pl"	
					renewal_datetime = field2datetime 	:field => "renewal date", 
														:data => @whois_data,
														:re => re,
														:strtime => strtime
					option_expiration_datetime = field2datetime 	:field => "option expiration date", 
																	:data => @whois_data,
																	:re => re,
																	:strtime => strtime
				end
				expiration_datetime = field2datetime 	:field => expiry_field, 
														:data => @whois_data,
														:re => re,
														:strtime => strtime

				
				if renewal_datetime != nil
					renewal_days_left = renewal_datetime.to_time - Time.now
					renewal_days_left = (renewal_days_left / (60 * 60 * 24)).ceil
				elsif expiration_datetime != nil
					expiration_days_left = expiration_datetime.to_time - Time.now
					expiration_days_left = (expiration_days_left / (60 * 60 * 24)).ceil			
				else
					@error_log.log("Neither renewal nor expiration time was not found for domain: #{@domain}")
				end
				
				if option_expiration_datetime != nil
					option_days_left = option_expiration_datetime.to_time - Time.now
					option_days_left = (option_days_left / (60 * 60 * 24)).ceil		
				elsif @whois_data =~ /no option/
					option_days_left = nil
				else
					if tld == "pl"
						@error_log.log("Option field not found for domain: #{@domain}")
					end
				end

				puts "  renewal_days_left: #{renewal_days_left}" if renewal_days_left != nil
				puts "  option_days_left: #{option_days_left}" if option_days_left != nil

				if (renewal_days_left != nil) && (expiration_days_left != nil)
					@error_log.log("Both renewal and expiration times were found for domain: #{@domain}")
				elsif (renewal_days_left != nil) || (expiration_days_left != nil)
					@days_left = renewal_days_left if renewal_days_left != nil
					@days_left = expiration_days_left if expiration_days_left != nil
					if (option_days_left != nil) && (option_days_left > @days_left)
						@days_left = option_days_left 
					end

				end
			elsif @available_present == true
				if @whois_data =~ Regexp.new("#{@domain}\\s+is\\s+available")
					@days_left = "available"
				else
					@error_log.log("Available parse failure for #{@domain}")
				end
			else
				@error_log.log("Unexpected whois format for  #{@domain}")
			end
			@whois_check_count += 1
			puts "    #{@whois_check_count} x whois check"
			
		end

	
		def field2datetime(args)
			datetime = nil
			options = Regexp::MULTILINE | Regexp::IGNORECASE
			if args[:data] =~ Regexp.new("#{args[:field]}", options)
		
				text = args[:data].gsub(Regexp.new("^(.+)(#{args[:field]}:[\\s\\w&:;\\.\\-]+)<br>(.*)$", options), '\2')
				text.gsub!(args[:re], '\1')
				text.gsub!(/\s+/, ' ')
				begin
					datetime = DateTime.strptime(text, args[:strtime])
				rescue ArgumentError
					@error_log.log("Unexpected date format for domain #{@domain}")
				end
			end

			return datetime
		end
		
		# def log_error(args)
			# File.open(@error_fn, "a+") {|file| file.puts args[:msg]}
		# end
end
