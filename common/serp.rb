
class Serp

	def initialize(args)
		@b = args[:browser]
		@error_log = args[:error_log]
	end
	
	def get_results(args)
		input = @b.text_field(:title, "Szukaj")
		button = @b.button(:value, "Szukaj")
		raise "Can't find search input field" if input == nil
		raise "Can't find search button" if button == nil

		args[:input] = input
		args[:button] = button
		
		if args[:domain] != nil
			res = search_domain args
		elsif args[:text] != nil
			res = search_text args
		else
			raise "unrecognized get results request"
		end
		
		res
	end	

	private
	
		def search_domain(args)
			results = { matches: false, entries: []}
			entries = []
			
			domain = args[:domain]
			search_text = domain.gsub(/^([\w\-]+)\..+$/, '\1')
			args[:input].set search_text
			args[:button].click
			sleep(2)
			check_suggestion search_text: search_text
			
			h3s = @b.h3s
			h3s.each_with_index do |h3, idx|
				h3.links.each do |l|
					begin
						http = HttpAddress.new(l.href)
					rescue
						next
					end
					match = http.matches?(domain)
					results[:matches] = true if match
					results[:entries] << "[#{idx}] #{(match ? " * ":"   ")} #{http.root}:  #{l.href} : #{match}"
				end
			end
			
			if args[:double_page] == true
			
			end
			
			
			results
		end
		
		def search_text(args)
			results = { matches: false, entries: []}
			entries = []
			
			args[:input].set args[:text]
			args[:button].click
			sleep(2)
			check_suggestion search_text: args[:text]
			
			h3s = @b.h3s
			h3s.each_with_index do |h3, idx|
				h3.links.each do |l|
					begin
						http = HttpAddress.new(l.href)
					rescue
						@error_log.log("Error processing address: #{l.href}")
						next
					end
					
					if h3.parent.class_name == "ads-ad"
						if l.style != "display: none;"
							results[:entries] << "[#{idx}]  A  #{http.root}:  #{l.href}"
						end
					else
						results[:entries] << "[#{idx}]     #{http.root}:  #{l.href}"
					end
				end
			end
			
			results
		end
	
		def check_suggestion(args)
			link = @b.link(:text, Regexp.new("^#{args[:search_text]}$"))
			
			begin
				if link.exists?
	# puts link.inspect
					span = link.parent.span(:class, "spell_orig")
					if span.exists?
						alternative_text = span.text
						if alternative_text != "Zamiast tego wyszukaj"
							@error_log.log("Suggestion link present however alternative text \
'#{alternative_text}' is different from 'Zamiast tego wyszukaj' for search text \
'#{args[:search_text]}'")
						end
						link.click
						sleep(2)
					else
						@error_log.log("Suggestion link present however description not \
found for search text '#{args[:search_text]}'")
					end
				end
			rescue Watir::Exception::MissingWayOfFindingObjectException
				@error_log.log("Problem with '#{args[:search_text]}'")	
			end
		end
end