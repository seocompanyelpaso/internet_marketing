#!/usr/bin/ruby

class Logger
	
	def initialize(args)
		@fn = args[:fn]
	end
	
	def log(msg)
		File.open(@fn, "a+") {|f| f.puts msg}
	end
end


class Utils

	def Utils.f2s(float, size_pre, size_post, prefix=" ")
		format_str = "%.#{size_post}f"
		str = "#{format_str % float}"
		diff = (size_pre + 1 + size_post) - str.length
		return "" << ((diff > 0) ? prefix * diff : "") << str
	end
	
	def Utils.i2s(int, size, prefix=" ")
		str = int.to_s
		diff = size - str.length
		return "" << ((diff > 0) ? prefix * diff : "") << str
	end
    
    def Utils.fill_str(str, size, options = {symbol: " ", orientation: :right})
        
        diff = size - str.size
        if diff > 0 
            if options[:orientation] == :right
                new_str = "#{str}#{options[:symbol] * diff}"
            else
                new_str = "#{options[:symbol] * diff}#{str}"
            end
        else
            new_str = "#{str}"
        end
        new_str
    end
	
    # austenitic_steel_18_8 -> AusteniticSteel18_8
    def Utils.camelize(str)
        new = str.gsub(/(_|\b)([a-z])/) {$2.upcase}
        # new.gsub!(/(\d)(_)(\d)/) {$1 << "-" << $3}
        new.gsub!(/([A-za-z])(_)(\d)/) {"" << $1 << $3}
        new
    end
    
	def Utils.color_to_rgb(color)
		map = {}
		map[:red] = "FF0000"
		map[:lime] = "00FF00"
		map[:blue] = "0000FF"
		map[:black] = "000000"
		map[:white] = "FFFFFF"
		map[:yellow] = "FFFF00"
		return map[color]
	end
    
    def Utils.add_suffix_to_fn(args)
        fn = args[:file_name]
        fn.gsub(/(\.[\d\w\-_]+$)/, "#{args[:suffix]}" << '\1')
    end
    
    def Utils.strip_fn_ext(file_name)
        file_name.gsub(/(.+)(\.[\d\w\-_]+$)/, '\1')
    end
    
    def Utils.strip_fn_prefix(file_name)
        file_name.gsub(/^([\d\w\-_]+\.)(.+)$/, '\2')
    end
    
    def Utils.get_fn(path)
        path.gsub(/^.*(\\|\/)([\d\w\-_.]+)$/, '\2')   
    end
    
    def Utils.split_path(path)
        components = {}
        if path =~ /^(.*\\|\/)([\d\w\-_.]+)$/
            components[:path] = $1
            components[:file] = $2
        end
        components
    end
    
    def Utils.valid?(args)
        valid = false
        if args[:sym] != nil
            if !args[:sym].is_a?(Symbol)
                raise "Symbol expected instead of #{args[:num].class}" 
            end
            args[:values].each {|value| valid = true if args[:sym] == value}
        elsif args[:num] != nil
            if !args[:num].is_a?(Numeric)
                raise "Numerical expected instead of #{args[:num].class}" 
            end
            args[:values].each {|value| valid = true if args[:num] == value}
        else
            raise "Validation of unsupported object attempted"
        end
        valid
    end
end

